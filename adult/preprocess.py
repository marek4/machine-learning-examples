import csv
import pandas as pd

class Adult:
    def __init__(self):
        self.data = {'Holand-Netherlands': 0.}

    def set_continous(self, name, value):
        self.data[name] = float(value.strip())

    def set_categorical(self, name):
        name = name.strip()
        if name != '?':
            self.data[name] = 1.

    def getData(self):
        return self.data

def preprocess_data(filename):
    adults = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            try:
                adult = Adult()

                adult.set_continous('Age', row[0])
                adult.set_categorical(row[1])
                adult.set_continous('Education', row[4])
                adult.set_categorical(row[5])
                adult.set_categorical(row[6])
                adult.set_categorical(row[7])
                adult.set_categorical(row[8])
                adult.set_categorical(row[9])
                adult.set_continous('Capital-gain', row[10])
                adult.set_continous('Capital-loss', row[11])
                adult.set_continous('Hours-per-week', row[12])
                adult.set_categorical(row[13])
                if (row[14].strip()[0] == '>'):
                    adult.set_categorical('Income')

                adults.append(adult.getData())
            except IndexError:
                continue
            except ValueError:
                continue

    df = pd.DataFrame(adults)
    df.fillna(0., inplace=True)

    return df