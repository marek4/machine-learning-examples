from preprocess import preprocess_data
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

target = 'Income'
features = ['Capital-gain', 'Capital-loss']

train = preprocess_data('adult.data')
test = preprocess_data('adult.test')

X_train, y_train = train[features].values, train[target].values
X_test, y_test = test[features].values, test[target].values

from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

import keras
from keras.models import Sequential
from keras.layers import Dense

# Initialising the ANN
classifier = Sequential()

# Adding the input layer and the first hidden layer
classifier.add(Dense(units = len(features), kernel_initializer = 'uniform', activation = 'relu', input_dim = len(features)))

# Adding the second hidden layer
# classifier.add(Dense(units = len(features), kernel_initializer = 'uniform', activation = 'relu'))

# Adding the output layer
classifier.add(Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid'))

# Compiling the ANN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Fitting the ANN to the Training set
classifier.fit(X_train, y_train, batch_size = 100, epochs = 10)

# Part 3 - Making predictions and evaluating the model

# Predicting the Test set results
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)

print(accuracy_score(y_test, y_pred))
print(f1_score(y_test, y_pred))