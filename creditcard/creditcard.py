import pandas as pd

data = pd.read_csv('creditcard.csv')
target = 'Class'
from sklearn.preprocessing import StandardScaler
data['normAmount'] = StandardScaler().fit_transform(data['Amount'].values.reshape(-1, 1))
X = data.drop([target, 'Time', 'Amount'], axis=1).values
y = data[target].values

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)


# Compare classifiers
def auc(y_true, y_pred):
    from sklearn import metrics
    fpr, tpr, thresholds = metrics.roc_curve(y_true, y_pred)
    return metrics.auc(fpr, tpr)

from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
classifiers = [KNeighborsClassifier(), DecisionTreeClassifier(), RandomForestClassifier(), GaussianNB()]

for clf in classifiers:
    clf = clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    print(clf.__class__)
    from sklearn import metrics
    print(metrics.accuracy_score(y_test, y_pred))
    print(metrics.recall_score(y_test, y_pred))
    print(auc(y_test, y_pred))
    print(metrics.confusion_matrix(y_test, y_pred))
    print()